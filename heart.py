#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

import numpy as np
import sounddevice as sd
#import matplotlib.pyplot as plt
import scipy.io.wavfile as wf
import thyaudio


def swave(f, det=0):
    return np.sin(x * (f + det)) * 0.5


bars_per_minute = 60
sample_rate = 44100
duration = 4

# for loop params
log_start = 3
log_end = 3.01
factor_log_base = 2.08
iter = 3
detune_l = 0
detune_r = 1.4

# factor array values multiplied by this
ffl_note = 20
freq_factor_left = 55 * (2**(ffl_note / 12))
ffr_note = 12
freq_factor_right = 55 * (2**(ffr_note / 12))

# if added to triangle wave
add_triangle = False
tri_note = 5
tri_factor = (2**(tri_note / 12))

# Do you want beat or raw sound
beatings = True

bars_per_minute_samples = int(sample_rate * 60 / bars_per_minute)
x = np.linspace(0, 2 * np.pi * duration, int(duration * sample_rate))
ramp_0 = np.logspace(1, -1, np.size(x), base=9) * 6
ramp_1 = np.logspace(-1, 2.8, np.size(x), base=8) * 6
#lfo = np.sin(x * 0.5) * 10

if add_triangle:
    #y_left = 2 / np.pi * np.arcsin(np.sin((40) * x)) * 0.6
    y_left = 2 / np.pi * np.arcsin(np.sin((110 * tri_factor) * x + ramp_1 * (np.sin(x * 220 * tri_factor) * 0.8))) * 0.3
    y_right = 2 / np.pi * np.arcsin(np.sin((222 * tri_factor) * x + ramp_0 * (np.sin(220 * x * tri_factor) * 0.5))) * 0.3
else:
    y_left = np.zeros(np.size(x))
    y_right = np.zeros(np.size(x))

factor = np.logspace(log_start, log_end, iter, base=factor_log_base)
fator = np.flip(factor)
for i in range(iter):
    y_left = y_left + swave(factor[i] * freq_factor_left, detune_l)
for i in range(iter):
    y_right = y_right + swave(factor[i] * freq_factor_right, detune_r)


y_left = y_left / np.max(np.abs(y_left))
y_right = y_right / np.max(np.abs(y_right))

if beatings:
    sound_beat0 = thyaudio.ThyRythmer(data=y_left, bar_size=bars_per_minute_samples,
                            decay_min=-2, flip=False, decay_log_base=10,
                            pattern=[1, 0, 1, 1, 0, 0, 0, 0])
    #sound_beat0.flip = True
    sound_l = sound_beat0.beater()
    sound_l = sound_l / np.max(np.abs(sound_l))

    sound_beat1 = thyaudio.ThyRythmer(data=y_right, bar_size=bars_per_minute_samples,
                            decay_min=-2, flip=False, decay_log_base=10,
                            pattern=[1, 0, 1, 0, 0, 0, 0, 0])
    #sound_beat1.flip = True
    sound_r = sound_beat1.beater()
    sound_r = sound_r / np.max(np.abs(sound_r))

    ys = np.vstack((sound_l, sound_r)).T
else:
    ys = np.vstack((y_left, y_right)).T

print(f"Array shape: {np.shape(ys)}")

sd.play(ys * 0.5, sample_rate)
sd.wait()

# write_data = np.int16(ys * 32767)
# wf.write('audio/wed_heart_.wav', sample_rate, write_data)

# plt. figure(figsize=(10, 4))
# plt.plot(y_left[:4000])
# plt.show()
