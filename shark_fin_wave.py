#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0


import numpy as np
import matplotlib.pyplot as plt
import sounddevice as sd
import scipy.signal as sig


def sharkfin(freq, form=0.9):
    y = sig.sawtooth(x * freq, form) + 1.1
    y = np.log10(y) * 0.2
    return y + 0.1


sample_rate = 44100
duration = 5.0

x = np.linspace(0, duration * 2 * np.pi, int(duration * sample_rate))
result = (sharkfin(440.0) + sharkfin(443.0) +
          sharkfin(444.0) + sharkfin(450.0)) * 0.3


sd.play(result, sample_rate)
sd.wait()

fig, ax = plt.subplots(figsize=(12, 4))
ax.set_title("Sharkfin wave with detune", fontsize=13)
ax.set_xlabel("Samples", fontsize=12)
ax.set_ylabel("Amplitude", fontsize=12)
ax.set_facecolor("#9ef7f1")
fig.subplots_adjust(bottom=0.2)

ax.plot(result[:1000], "blue")
plt.show()

# How to use your own fonts:-
# https://matplotlib.org/3.1.0/gallery/text_labels_and_annotations/font_file.html
