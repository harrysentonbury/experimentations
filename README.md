# Experimentations

**Experiments with audio and audio synthesis using python3**

```
pip3 install numpy scipy simpleaudio sounddevice matplotlib
```

**To load mp3 files into a numpy arrays:**
```
pip3 install audio2numpy
```

**audio2numpy requires ffmpeg**
```
sudo apt install ffmpeg
```

## breather
Sounds like creepy breathing.

## brown
output sound A or sound B depending on the amplitude of the input sound file.
(sound A being from the input file in this case, sound B is some audio FM)

## december_wave
Some code that returns a piece of music.

## envelope_grabber
Returns the envelope or the inverted envelope of a sound file (0 to 1.0).
This can be used to multiply another sound with amongst other things.
This code is an example of shaping some added together triangle waves.
Stereo or mono input. Outputs mono.

## fractaling
Generate Mandelbrot Set fractals. It is not audio stuff but i will just keep it
here for now. Can run in the terminal.
```
python3 fractaling.py
```
Example command with times 5
magnification (zoom) and -1.5  x offset and 0.0 y offset and also save a png image:-
```
python3 fractaling.py -m 5 -x -1.5 0.0 -o mandelbrot.png
```
Further investigation [Here](https://harrysentonbury.codeberg.page/fractaling.html).
The idea is to open it in a text editor and experiment with operations on the RGB
to get differend colouring. **np.uint8(*whatever operation* to index)** on lines
55, 56 and 57.

## heart
It originally made heart beat sounds but then it evolved. It now makes all sorts
of beats and noises.

## j4
Weird noise. Involves changing speed of a filtered, frequency modulated but
phase shifted by an integer value radians incremented as a function of x sine wave.

## loopy_code_woteva
That crap from [diode.zone](https://diode.zone/w/1MtDqEjn9yKbdRHYQSKTA7).
You will need to provide some input for the code to mess with, it will appear in
the left channel.
Run: just say or sing stuff or woteva into you default mic:-
```
python3 loopy_code_woteva.py
```
Or use a sound file:-
```
python3 loopy_code_woteva.py -i [path/to/file.wav or mp3]
```
Change the default bars per minute:-
```
python3 loopy_code_woteva.py -b [integer, bars per minute. default=43]
```

## oscillogram
View what small sound files look like. Mono, stereo, print number of samples,
sample rate and duration. wav or mp3.

To zoom Click on the magnifier icon then click and drag a rectangle
around the section to zoom into, then use the arrow keys to toggle between
views. Click on the move icon and pan with left click and drag, increase
amplitude with right click and drag. The window is also resizable.


**Prerequisits**

```
pip3 install numpy matplotlib scipy audio2numpy
```

**Run**

```
python3 oscillogram.py /path/to/file.wav
```

## phlazinger
A phaser or flanger or whatever. sin or sweep control or custom signal of the same
length numpy array as the input sound. ie:- an envelope from **envelope_grabber**.
Iterate effect over sound n times and delay on each iteration by n samples.

## quarter dead synth
An earlier incarnation of
[cutie-dead-synth](https://codeberg.org/harrysentonbury/cutie-dead-synth) using
tkinter instead of PyQt. I called it 1/4 dead because that was the first
tune that popped out of my fingers after I tapped a key and heard the first note.
It uses the **simpleaudio** package. I added a button to create a directory
full of wav files of each note. Type the required
sample rate as an arg when running. Defualts to 48000 Hz with no arg.

**Prerequisites for simpleaudio**

```
sudo pip3 install --upgrade pip setuptools
sudo apt-get install -y python3-dev libasound2-dev
```

**dependences**

```
pip3 install numpy scipy
pip3 install simpleaudio

sudo apt-get install python-tk
```

## random_audio_slices
Chops up a numpy array full of audio data then sticks it back together in a
random order.

## shaped_filtered_square
Added together square waves, high pass filtered then shaped into a rhythm.

## simmer_bass
Iterating filters on shark fin waves in envelopes. Used in a music video:-
[diode.zone-simmer()](https://diode.zone/videos/watch/04c5f4a9-e6dd-452f-ad7f-473939ab39a2)

## sound2video_effects
Something i have started experimenting with. I will leave it here so i don't forget.
I use the program called **view_shit.py** to see the audio file values.

## stretchinger
It stretches and stretches and stretches... Or squishes sound. Outputs stereo.

## shark_fin_wave
A wave that looks like a shark fin. When detuned, it evolves into a shark shape.
*Excuse: I was bored!.*

## thyaudio
All the classes from these experiments in one place. Depends on numpy.

## wave_folding
Folds any wave or sound file you can fit into a numpy array. This includes .wav
files etc. Folds whatever half of the wave you like, as many times you like.

__________________________________

Some of this stuff was used and can be heard in some videos on my
[diode zone](https://diode.zone/video-channels/harrys_stuff/videos) channel.
It is just low quality messing around and stuff.
