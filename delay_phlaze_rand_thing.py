#!/usr/bin/env python3

# Copyright (C) 2022 harrysentonbury
# GNU General Public License v3.0


from audio2numpy import open_audio
import numpy as np
import sounddevice as sd
import scipy.io.wavfile as wf
import thyaudio as ta



class ThyDelayPhlaze():
    """
    sound - 1d numpy array
    sample_rate - int, sample rate of sound array
    delay_time - float, a positive in seconds, default=0.2
    repeats - int, a positive number of delays, default=5
    decay - float, amount of decay of delay 1=no delay, default=2
    reverse - boolean, reverse the returned sound, default=False
    hanning - boolean, apply hanning_window to repeats, default=False
              !hanning applied to whole input array length.
    ramp - boolean, apply log ramp to input array, default=False
    wot - string, what happens to delayed sound, default="nothing"
          other options are "phlaze" or "randomer"
    """


    def __init__(self, sound, sample_rate, delay_time=0.2, repeats=5, decay=2, reverse=False, hanning=False, ramp=False, wot=""):
        self.sound = sound
        self.sample_rate = sample_rate
        self.delay_time = delay_time
        self.repeats = repeats
        self.decay = decay
        self.reverse = reverse
        self.hanning = hanning
        self.ramp = ramp
        self.log_base = 7
        self.snd_size = np.size(self.sound, axis=0)
        self.wot = wot


    def phlazers(self, i):
        self.arr[i, i * self.blox:i * self.blox + self.snd_size] = (
        ta.ThyPhlazer(self.sound, sample_rate=self.sample_rate, loops=3,
        speed=0.1 + (i/2), depth=10 * i).phlaze() * 1 / (self.decay**i))


    def randomers(self, i):
        self.random = ta.ThyRandomAudioSlices(self.sound, window_size=2**13, flip=True)
        self.arr[i, i * self.blox:i * self.blox + self.snd_size] = self.random.chop_and_chuck()[:self.snd_size] * 1 / (self.decay**i)


    def delayer(self):
        self.sound = self.sound / np.max(np.abs(self.sound))
        if self.reverse:
            self.reverse = np.flip(self.reverse)

        self.blox = round(self.delay_time * self.sample_rate)
        self.sound = self.sound.reshape(self.snd_size,)
        self.arr = np.zeros((self.repeats, (self.snd_size) + self.blox * self.repeats))
        self.log_ramp = np.logspace(0, 1, self.snd_size, base=self.log_base) / self.log_base
        for i in range(0, self.repeats):
            if self.wot ==  "phlaze":
                if i > 0:
                    self.phlazers(i)
                else:
                    self.arr[i, i * self.blox:i * self.blox + self.snd_size] = (self.sound * 1 / (self.decay**i))
            elif self.wot == "randomer":
                if i > 0:
                    self.randomers(i)
                else:
                    self.arr[i, i * self.blox:i * self.blox + self.snd_size] = (self.sound * 1 / (self.decay**i))
            else:
                self.arr[i, i * self.blox:i * self.blox + self.snd_size] = (self.sound * 1 / (self.decay**i))
            if self.hanning:
                self.sound *= np.hanning(self.snd_size)
            if self.ramp and not self.hanning:
                self.sound *= self.log_ramp

        self.result = np.sum(self.arr, axis=0)
        self.result = self.result / np.max(np.abs(self.result))
        if self.reverse:
            self.result = np.flip(self.result)
        print(f"Return Shape: {np.shape(self.result)}")
        return self.result



# Read input wav or mp3 file.
path_to_file = "audio/cds_det2_ra122_a15.mp3"

if path_to_file.endswith(".mp3"):
    sound, sample_rate = open_audio(path_to_file)
    sound = np.float64(sound)

else:
    sample_rate, sound = wf.read(path_to_file)
    sound = np.float64(sound)

# If stereo convert to mono
try:
    sound = sound[:, 0] + sound[:, 1]
    print("stereo to MONO")
except IndexError:
    print("MONO")

print(f"In file shape: {sound.shape}")
print(f"Samplerate: {sample_rate}")
repeats = 30
delay_time = 0.5    # 1.5 in that vid
fade = 1
flip = 0
hanning = 0
ramp = 0
wot = "phlaze"

end_result = ThyDelayPhlaze(sound, sample_rate=sample_rate, delay_time=delay_time,
                      repeats=repeats, decay=fade, reverse=flip, hanning=hanning, ramp=ramp, wot=wot).delayer()
#end_result = ta.ThyPhlazer(end_result, sample_rate, sweep=0, speed=0.01, phase=1, loops=3).phlaze()


sd.play(end_result, sample_rate)
sd.wait()
