#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0


import numpy as np
import scipy.io.wavfile as wf
import sounddevice as sd
import time


def noise():
    y = np.random.normal(0, 0.4, arr_size,)
    y = np.clip(y, a_min=-1.0, a_max=1.0) * 0.5
    return y


def wave_maker(frequency, woteva=0):
    y = np.sin(x * frequency + woteva)
    return y


sample_rate = 44100
duration = 30
arr_size = int(sample_rate * duration)
x = np.linspace(0, duration * 2 * np.pi, arr_size)
ramp_0 = np.logspace(-1, 1, arr_size, base=7)

trem_amount_value = 0.7
trem_adder = 1.0 - trem_amount_value
tremolo_0 = (wave_maker(0.5) * trem_amount_value) + trem_adder
tremolo_1 = (wave_maker(0.25) + 1) / 2

sound_0 = wave_maker(220, ramp_0 * wave_maker(330)) * 0.5
sound_1 = wave_maker(111, ramp_0 * wave_maker(333)) * 0.5
noiser = noise()

l_play = (noiser - sound_0) * tremolo_0 * 0.15
r_play = (noiser - sound_1) * tremolo_0 * 0.15
r_play = np.roll(r_play, 3000)
play_stereo = np.vstack((l_play, r_play)).T

sd.play(play_stereo, sample_rate)
sd.wait()

# wf.write("breath_0.wav", sample_rate, play_stereo)
