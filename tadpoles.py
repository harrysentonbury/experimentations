#!/usr/bin/env python3

# Copyright (C) 2022 harrysentonbury
# GNU General Public License v3.0

import numpy as np
import scipy.signal as sig
import scipy.io.wavfile as wf
import sounddevice as sd
import thyaudio as ta


def lfo(speed=0.5, amp=2, phase=0):
    return (np.sin((x * speed) + phase) + amp)


def filtering(data, filter_type="low", cr=(1000)):
    sos1 = sig.butter(10, cr, btype=filter_type, fs=sample_rate, output="sos")
    filt = sig.sosfilt(sos1, data)
    return filt


def builder(n):
    ramp_0 = np.logspace(1, -4, np.size(x), base=2)
    y0 = np.sin(x * 220 + lfo() * np.sin(x * 110)) + np.sin(x * 444 + lfo(0.25, 4) \
     * np.sin(x * 56)) + (np.sin(x * 880) * 0.1) + (sig.sawtooth(x * 888 + lfo(amp=10) * np.sin(x * popo * 2), 0.8) * 0.3)
    y0 = filtering(y0, "low", n * 70)
    y1 = sig.sawtooth(x * popo / 2, 0.7) + (sig.sawtooth(x * popo, 0.2) * 0.5)
    # y1 = filtering(y0, "bandpass", (n * 20, n * 150))
    y1 = ta.ThyFoldinger(y1, 0.5).folding()
    y1 = ta.ThyFoldinger(y1, 0.5, positive=False).folding()
    y1 = ta.ThyPhlazer(y1, sample_rate, loops=15, speed=0.5, depth=150, loop_delay=3).phlaze()
    return y0 + y1


sample_rate = 41100
duration = 4
popo = 110 * (2**(0/12))
x = np.linspace(0, 2 * np.pi * duration, int(sample_rate * duration))

y = np.array([])
for i in range(1, 25):
    y = np.concatenate((y, builder(i)))
y = y + (np.roll(y, 300) * 0.6)

y = y / np.max(np.abs(y))
ys = np.vstack((y, np.roll(y, 3000))).T

sd.play(ys * 0.3, sample_rate)
sd.wait()

# ys_int16 = np.int16(32767 * ys)
# wf.write("audio/gob_l.wav", sample_rate, ys_int16)
