#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

import numpy as np
import scipy.signal as sig
import scipy.io.wavfile as wf
import sounddevice as sd


def speeder(data, d_factor):
    indices = np.round(np.arange(0, np.size(data, axis=0), d_factor))
    indices = indices[indices < np.size(data, axis=0)].astype(int)
    return data[indices]

def filtering_butter(data, c_f):
    sos1 = sig.butter(9, c_f, btype='low', fs=sample_rate, output='sos')
    filt = sig.sosfilt(sos1, data)
    return filt


sample_rate = 48000
duration = 5.0
critical_f = 10000      # low pass filter
speed_factor_0 = 0.2    # gonna slow it down
speed_factor_1 = 0.25
roll_amount = 10000      # samples delay

# build the sound wave
x = np.linspace(0, 2 * np.pi * duration, int(duration * sample_rate))
x_int = np.arange(int(duration * sample_rate))
lfo_xint = np.sin(x_int * 0.15)
lfo = (np.sin(x * 0.5) + 1)
data = np.sin(x * (220 + lfo_xint)  + lfo * (np.sin(x * 2) * 0.9)) * 0.02
data1 = np.sin(x * (113 + lfo_xint)  + lfo * (np.sin(x * 2) * 0.9)) * 0.02

# slow it down
data = speeder(data, speed_factor_0)
data1 = speeder(data1, speed_factor_0)

# filtering
data = filtering_butter(data, critical_f)
data1 = filtering_butter(data1, critical_f)

data = (data1 + data)
data = np.concatenate((data, np.flip(data)))

# make stereo and delay a channel
data_roll = np.roll(data, roll_amount)
data_roll[:roll_amount] = 0
data_roll2 = np.roll(data_roll, roll_amount)
data = data + (data_roll2 * 0.7)
data_roll = data_roll + (data_roll2 * 0.7)
data_stereo = np.vstack((data, data_roll)).T

sd.play(data_stereo, sample_rate)
sd.wait()
