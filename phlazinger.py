# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

from audio2numpy import open_audio
import numpy as np
import scipy.io.wavfile as wf
import sounddevice as sd

import thyaudio as ta


path_to_file = "audio/brown_8.mp3"

# read mp3 or wav with appropriate package.
if path_to_file.endswith(".mp3"):
    data, sample_rate = open_audio(path_to_file)
else:
    sample_rate, data = wf.read(path_to_file)
data = np.float64(data)

# process two tracks seperately, even if mono
try:
    if data.shape[1] == 2:
        data_l = data[:, 0]
        data_r = data[:, 1]
        print("stereo input file")
except IndexError:
    data_l = data[:]
    data_r = data[:]
    print("mono input file")


# double this small sound file a few of times
for _ in range(3):
    data_l = np.concatenate((data_l, data_l))
    data_r = np.concatenate((data_r, data_r))

# chop up and shuffle
window_size = 2**15
shuffle_object_l = ta.ThyRandomAudioSlices(data_l, window_size, fade_length=500)
data_l = shuffle_object_l.chop_and_chuck()


shuffle_object_r = ta.ThyRandomAudioSlices(data_r, window_size, fade_length=500)
data_r = shuffle_object_r.chop_and_chuck()
data_r = np.roll(data_r, 8000)

# phlazing
num_loops = 4
phase_l = ta.ThyPhlazer(data_l, sample_rate=sample_rate, loops=num_loops, loop_delay=4000)
phase_l.depth=200
phase_l.sweep=True
phase_l.speed = 0.5
left_channel = phase_l.phlaze()

phase_r = ta.ThyPhlazer(data_r, sample_rate=sample_rate, loops=num_loops, loop_delay=6000)
phase_r.depth=1000
phase_r.sweep=True
phase_r.speed = 0.5
phase_r.phase = np.pi / 12
right_channel = phase_r.phlaze()

# back into stereo
stereo_sound = np.vstack((left_channel, right_channel)).T

sd.play(stereo_sound, sample_rate)
sd.wait()

# thing = np.int16(stereo_sound * 32767)
# wf.write("a_phlazed_noise.wav", sample_rate, thing)
