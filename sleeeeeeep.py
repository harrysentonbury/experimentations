# Copyright (C) 2024 harrysentonbury
# GNU General Public License v3.0

import math
import pygame

## Colors
black = (0, 0, 0)
ball_color = (200, 0, 50)
line_color = (100, 100, 100)
circle_color = (25, 25, 25)

width, height = 900, 700

pygame.init()
window = pygame.display.set_mode((width, height))
pygame.display.set_caption("Sleeeeeeeep")
# icon = pygame.image.load("./assets/black_icon.png")
# pygame.display.set_icon(icon)
clock = pygame.time.Clock()

def swing(mid, pivot_height=0):
    pygame.draw.line(window, line_color, (mid_point, pivot_height),
                     (xy_pos[0], xy_pos[1] + y_offset), 4)
    pygame.draw.circle(window, ball_color, (round(xy_pos[0]),
                       round(xy_pos[1] + y_offset)), 15)

    pygame.draw.circle(window, (100, 0, 0), (round(xy_pos[0]),
                       round(xy_pos[1] + y_offset)), 75, width=7)
    pygame.draw.circle(window, circle_color, (mid_point,
                       y_offset), swing_radius, width=3)
    pygame.draw.circle(window, line_color, (150, 175), 75, width=7,
                       draw_bottom_left=True, draw_bottom_right=True)
    pygame.draw.circle(window, line_color, (width - 150, 175), 75, width=7,
                       draw_bottom_left=True, draw_bottom_right=True)
    if hit_midpoint:
        pygame.draw.line(window, (200, 200, 200), (mid_point, 0),
                         (mid_point, height), width=3)


def move(theta, swing_radius):
    xy_pos[0] = mid_point + (swing_radius * math.sin(theta))
    xy_pos[1] = swing_radius * math.cos(theta)


theta = 0
vel = 0
acc = 0

y_offset = 200
pivot_height = y_offset + 0
mid_point = width // 2

hit_midpoint = False
midpoint_thresh = 6
xy_pos = [80, 100]

swing_radius = math.sqrt(math.pow(mid_point - xy_pos[0], 2) + math.pow(xy_pos[1], 2))
theta = math.asin((xy_pos[0] - mid_point) / swing_radius)

count = 0

run = True
while run:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    window.fill(black)

    acc = - 0.0005 * math.sin(theta)
    vel += acc
    theta += vel
    move(theta, swing_radius)
    if round(xy_pos[0]) >= mid_point - midpoint_thresh and round(xy_pos[0])\
            <= mid_point + midpoint_thresh:
        if hit_midpoint is False:
            hit_midpoint = True
    if round(xy_pos[0]) < mid_point - midpoint_thresh - 1 or round(xy_pos[0])\
            > mid_point + midpoint_thresh + 1:
        hit_midpoint = False
    swing(mid_point, pivot_height)

    pygame.display.flip()

