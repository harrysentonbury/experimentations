#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0


import numpy as np
import scipy.signal as sig
import simpleaudio as sa

import thyaudio as ta


def noise():
    y = np.random.normal(0, 0.4, arr_size,)
    y = np.clip(y, a_min=-1.0, a_max=1.0)
    return y


def filtering_butter(data, cru, btype="low"):
    sos1 = sig.butter(10, cru, btype=btype, fs=sample_rate, output='sos')
    filt = sig.sosfilt(sos1, data)
    return filt


# def sharkfin(freq, form=0.9):
#     "Silly - A bit plussy"
#     y = sig.sawtooth(x * freq, form) + 1.1
#     y = np.log10(y) * 0.2
#     return y + 0.1


def sharkfin(freq, form=0.9):
    "Sensible"
    y = sig.sawtooth(x * freq, form)
    return y


def loops(arr, start=2, stop=6, step=1):
    sound = np.array([])
    for i in range(start, stop, step):
        cr = i * 250
        #cr1 = cr + 800     # if btype="bandpass" cr must be tuple ie (cr, cr1)
        print(cr)
        yy = filtering_butter(arr, cr)

        sound_beat = ta.ThyRythmer(data=yy, bar_size=sample_rate, decay_min=-1,
                                flip=False, decay_log_base=5, attack=100,
                                pattern=[1, 1])

        sound = np.concatenate((sound, sound_beat.beater()))
    return sound


sample_rate = 44100
duration = 2
arr_size = int(sample_rate * duration)
x = np.linspace(0, duration * 2 * np.pi, arr_size)

ramp_0 = np.logspace(1, -1, arr_size, base=5)

y = (sharkfin(110.0) + sharkfin(112.0) +
          sharkfin(113.0) + np.sin(x * 55.0 + (np.sin(x * 51) * 0.5))) * 0.3

sound_array = loops(y, start=1, stop=6)
sound_array = np.concatenate((sound_array, loops(y, start=6, stop=1, step=-1)))

sound_array = np.int16(sound_array * 32767)
play_object = sa.play_buffer(sound_array, 1, 2, sample_rate)
play_object.wait_done()
