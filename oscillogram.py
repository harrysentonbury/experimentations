#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0

# Quickly look at an oscillogram of wav and mp3 files. Stereo or mono.
# Number of samples/frames and duration

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wf
import sys
plt.style.use("dark_background")

a2n = True
try:
    from audio2numpy import open_audio
except ModuleNotFoundError:
    a2n = False

try:
    def duration_m_s(samples):
        total_seconds = samples / sample_rate
        minutes = int(total_seconds // 60)
        seconds = total_seconds % 60
        return f"Duration {minutes}:{seconds:.4f}"


    if len(sys.argv) != 2:
        raise ValueError("Must provide path to file. Example oscillogram.py path/to/file.wav")
    path_to_file = sys.argv[1]
    if path_to_file.endswith(".wav"):
        sample_rate, data = wf.read(path_to_file)
    elif a2n and path_to_file.endswith(".mp3"):
        data, sample_rate = open_audio(path_to_file)
    else:
        sys.exit("Arguement must be path to wav file.")

    data = data / np.max(np.abs(data))

    label_color = "#00a0b3"
    if len(data.shape) == 2:
        number_of_samples = np.size(data[:, 0])
        fig, (ax0, ax1) = plt.subplots(2, 1, figsize=(8.5, 3), constrained_layout=True, sharex=True)
        ax0.set_ylim(-1.0, 1.0)
        ax0.tick_params(labelsize='small', width=1, labelcolor=label_color)
        # ax0.set_ylabel("Left", fontsize=8)
        ax0.plot(data[:, 0], linewidth=0.5)

        ax1.set_ylim(-1.0, 1.0)
        ax1.tick_params(labelsize='small', width=1, labelcolor=label_color)
        # ax1.set_ylabel("Right", fontsize=8)
        ax1.plot(data[:, 1], linewidth=0.5)
        plt.xlabel(f"{number_of_samples} Samples at ({sample_rate}Hz)   {duration_m_s(number_of_samples)}", fontsize=11)
        plt.show()
    else:
        number_of_samples = np.size(data)
        fig, ax0 = plt.subplots(figsize=(8.5, 2), constrained_layout=True)
        # ax0.set_ylabel("Mono", fontsize=8)
        ax0.set_ylim(-1.0, 1.0)
        ax0.tick_params(labelsize='small', width=1, labelcolor=label_color)
        ax0.plot(data, linewidth=0.5)
        plt.xlabel(f"{number_of_samples} Samples at ({sample_rate}Hz)   {duration_m_s(number_of_samples)}", fontsize=11)
        plt.show()
except Exception as e:
    sys.exit(f"{type(e).__name__}: {str(e)}")
