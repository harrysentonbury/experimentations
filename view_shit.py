from audio2numpy import open_audio
import numpy as np
import scipy.io.wavfile as wf
import scipy.signal as sig
import matplotlib.pyplot as plt

import thyaudio as ta


def butter_filter(data, cru, btype="low"):
    sos1 = sig.butter(10, cru, btype=btype, fs=sample_rate, output="sos")
    filt = sig.sosfilt(sos1, data)
    return filt


path_to_file = "path/to/audio_file.wav"

if path_to_file.endswith(".mp3"):
    sound, sample_rate = open_audio(path_to_file)

elif path_to_file.endswith(".wav"):
    sample_rate, sound = wf.read(path_to_file)

## if stereo to mono ##
try:
    sound = sound[:, 0] + sound[:, 1]
except IndexError:
    pass


sound = sound / np.max(np.abs(sound))

## In case it needs filtering, uncomment. cru=bandwith. btype also "low" or "high"
#sound = butter_filter(sound, cru=(70, 1000), btype="bandpass")
print(sound.shape)
print(f"rate {sample_rate}")

frame_rate = 25
frame_count = 500
samples = np.size(sound)
duration = samples / sample_rate
samples_per_frame = 1
new_samples_per_second = frame_rate * samples_per_frame
resize_samples = int(new_samples_per_second * duration)

print(f"sound duration: {duration} samples: {samples}")
print(f"new_samples_per_second: {new_samples_per_second}")
print(f"resize_samples: {new_samples_per_second * duration} int resize_samples {resize_samples}")

if resize_samples < frame_count:
    resize_samples = resize_samples + 1

envelope = ta.ThyEnveloper(sound, window_size=1024).get_envelope()
envelope = np.clip(envelope, 0.0, 0.4)

invert_envelope = ta.ThyEnveloper(sound, window_size=256, invert=True).get_envelope()
#invert_envelope = np.clip(invert_envelope, 0.0, 1.0)


resampled = (sig.resample(envelope, resize_samples) * 100)
resampled = np.clip(resampled, 0.0, 100)
invert_resampled = (sig.resample(invert_envelope, resize_samples) * 40)
print(f"resampled shape: {resampled.shape}")
resampled = np.uint8(resampled)
invert_resampled = np.uint8(invert_resampled)
#invert_resampled = np.clip(invert_resampled, 20.0, 40.0)


fig, (ax0, ax1, ax2, ax3) = plt.subplots(4, 1, figsize=(12, 12))
ax0.set_title("Sound", fontsize=13)
ax0.plot(sound)

ax1.set_title("Envelope", fontsize=13)
ax1.plot(envelope)

ax2.set_title("Resampled", fontsize=13)
ax2.plot(resampled)

ax3.set_title("Invert Resampled", fontsize=13)
ax3.plot(invert_resampled)

plt.tight_layout()
plt.show()
