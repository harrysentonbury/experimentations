#!/usr/bin/env python3

# Copyright (C) 2021 harrysentonbury
# GNU General Public License v3.0


import numpy as np
import matplotlib.pyplot as plt
import simpleaudio as sa
plt.style.use("dark_background")


class ThyFoldinger():
    """ folds positive half or negitive half of waves.
        sound - numpy array
        threshold - folding at a point between 0 and 1, 1 being max amplitude.
        positive - boolean which half of wave to be folded. default=True.
    """
    def __init__(self, sound, threshold, positive=True):
        self.sound = sound
        self.threshold = threshold
        self.positive = positive
        if self.threshold > 1 or self.threshold < 0:
            raise ValueError("threshold must be  >= 0 and <=1")

    def folding(self):
        self.sound = self.sound / np.max(np.absolute(self.sound))
        if self.positive:
            self.sound = np.where(self.sound < self.threshold, self.sound,
                          -self.sound + (2 * self.threshold))
        else:
            self.sound = np.where(self.sound > (-self.threshold), self.sound,
                          -self.sound - (2 * self.threshold))
        return self.sound


def wave_maker(frequency, woteva=0):
    y = np.sin(x * frequency + woteva)
    return y


sample_rate = 44100
duration = 3
arr_size = int(sample_rate * duration)
x = np.linspace(0, duration * 2 * np.pi, arr_size)
ramp_0 = np.logspace(1, -1, arr_size)

sound = wave_maker(330, ramp_0 * wave_maker(60))

sound = ThyFoldinger(sound, threshold=0.8).folding()
sound = ThyFoldinger(sound, threshold=0.5, positive=False).folding()
sound = ThyFoldinger(sound, threshold=0.6).folding()
sound = sound * 0.3

sound = np.int16(sound * 32767)
play_object = sa.play_buffer(sound, 1, 2, sample_rate)
play_object.wait_done()

plt.figure(figsize=(12, 4))
plt.title("Some wave folding")
plt.plot(sound[:300])
plt.show()
