#!/usr/bin/env python3

# Copyright (C) 2022 harrysentonbury
# GNU General Public License v3.0

from audio2numpy import open_audio
import numpy as np
import scipy.io.wavfile as wf
import scipy.signal as sig
import sounddevice as sd

import thyaudio as ta


def folphlaze(fdata, phlaze=False):
    thr=0.2
    fdata = fdata / np.max(np.abs(fdata))
    fdata = ta.ThyFoldinger(fdata, threshold=thr).folding()
    fdata = ta.ThyFoldinger(fdata, threshold=thr, positive=False).folding()
    if phlaze:
        fdata = ta.ThyPhlazer(fdata, sample_rate, loops=3, depth=50, speed=0.01,
                              phase=np.pi, sweep=False).phlaze()
    fdata = fdata / np.max(np.abs(fdata))
    return fdata

path_to_file = "audio/brown_8.mp3"

if path_to_file.endswith(".mp3"):
    data, sample_rate = open_audio(path_to_file)

elif path_to_file.endswith(".wav"):
    sample_rate, data = wf.read(path_to_file)

## if stereo to mono ##
try:
    data[:, 0] = data[:, 0] + data[:, 1]
    data = data[:, 0]
except IndexError:
    pass


print(f"Infile Shape: {data.shape}")
data = data / np.max(np.abs(data))
data_dry = data / np.max(np.abs(data))
arr_size = np.size(data, axis=0)

## Vars for echo
echo = True
reverse = False
## end_result vars
dry = 0.15
dry_roll_amount = -2000
extra = np.zeros(int(arr_size / 2))     # length of silence

end_result = np.array([])
for i in range(10, 14):
    squishie_factor = 1 + ((2**(i/12)) * i)
    #squishie_factor = 1 + (12 * i)
    #squishie_factor = 1 + ((2**(i/36)) * i)    # range(10, 16) sounds ok

    ## thingy ##
    indices = np.round(np.arange(0, np.size(data, axis=0), squishie_factor))
    indices = indices[indices < np.size(data, axis=0)].astype(int)
    result = data[indices]

    sound_resampled = sig.resample(result, arr_size)
    ## end thingy ##

    end_result = np.concatenate((end_result,
                                sound_resampled + (np.roll(data_dry, dry_roll_amount) * dry),
                                extra,
                                (folphlaze(sound_resampled, phlaze=False)) * 0.4)
                                )

if echo:
    end_result = ta.ThyDelay(end_result, sample_rate=sample_rate, delay_time=0.3,
                      repeats=8, decay=2, reverse=reverse).delayer()


#end_result = folphlaze(end_result, phlaze=True)
end_result = end_result / np.max(np.abs(end_result))
print(f"End result SHAPE: {np.shape(end_result)}")
sd.play(end_result * 0.5, sample_rate)
sd.wait()

# data_int16 = np.int16(end_result * 32767)
# wf.write("audio/brown_thingy_iter.wav", sample_rate, data_int16)
